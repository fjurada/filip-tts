/* Author : Filip Jurada
 */

#include <stdio.h>
#include <stdlib.h>
#include <sndfile.h>
#include <locale.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <wctype.h>
#include <wchar.h>

#define BUFFER_LEN      1024
#define MAX_CHANNELS    6

//Existing-used phonemes
char *phonemes[] = {"0", "A", "B", "C", "Č", "Ć", "D", \
				"#", "$", "E", "F", "G", "H", "I", \
				"J", "K", "L", "|", "M", "N", "%", \
				"{", "O", "P", "R", "&", "S", "Š", \
				"T", "U", "V", "Z", "Ž"};
/* Changes during text preparation: (SPECIAL CHARACTERS (č,ž,đ...) NOT YET WORKING)
 * DŽ = #
 * Đ = $
 * LJ = |
 * N2 = %
 * NJ = {
 * R2 = &
 * ' '= 0
 * ','= 0
 * '.'= 00 -?? maybe a problem - solved easily with creating new, 
 * 				longer recording of silence to replace this. For now using 0
 */

/* Function prototypes. */

//prepare string to start working with it
void prepare_text(char *text);

//Write a phoneme to a specific soundfile that is opened
unsigned int write_phoneme (char *phoneme, SNDFILE *outfile) ;

//Check if the phoneme exists
int isPhoneme(char *phon);

//Check if it is one of allowed interpuction characters
int isInterpunct(char c);

//Make one file with all indivudual phoneme LPC-s. The function needs to receive a string with only allowed characters
FILE *catLPCs(char *phons);

//Write byte file from path to the file output provided
unsigned int write_lpc (char *path, FILE *output);

//Make one pitch file derived from all phonemes used, and by using write pitch, modify anything that needs changing
FILE *catPitches(char *phons);

//Write phoneme pitch data to specified pitch file
unsigned int write_pitch (char *path, FILE *output);

int main(int argc, char *argv[])
{
	char *locale;
    locale = setlocale(LC_ALL, "hr_HR.UTF-8");
	
	if (argc != 2)
	{
		printf("You need only one argument! - String text to be converted to speech. \n");
		return -1;
	}
	
	char arg[100];
	char phnm[2];
	strcpy(arg, argv[1]);
	
	SNDFILE *infile, *outfile;
	FILE *lpcFile, *pitchFile;
	SF_INFO sfinfo_orig = {};
	SF_INFO* sfinfo = &sfinfo_orig; 

	//retrieve format data in sfinfo
	if (! (infile = sf_open ("0.wav", SFM_READ, sfinfo)))
    {   
		/* Open failed so print an error message. */
        printf ("Not able to retrieve format info.\n") ;
        /* Print the error message fron libsndfile. */
        sf_perror (NULL) ;
        return  1 ;
	}
	else
		printf("\nFormat info initialized sucessfully...");
	
	/* Open the output file. */
    if (! (outfile = sf_open("output.wav", SFM_WRITE , sfinfo)))
    {   printf ("Not able to open output file %s.\n", "output.wav") ;
        sf_perror (NULL) ;
        return  1 ;
    }
    else
		printf("\nOutput opened/created.");
	
	sf_close(infile);
	
	//prepare text from argument
	prepare_text(arg);
	printf("\nPrepared text: %s", arg);
	
	/*//collect all lpc data necessary
	lpcFile = catLPCs(arg);
	pitchFile = catPitches(arg);
	fclose(lpcFile);
	fclose(pitchFile);*/

	int i = 0;
	while (arg[i] != '\0')
	{
		phnm[0] = arg[i];
		phnm[1] = '\0';
		printf("\nNow should be writing phoneme %c.\n", arg[i]);
		write_phoneme(phnm, outfile);
		i++;
	}
	
	
	sf_close(outfile);
	//for (int i = 0; i<=200 ; i++) printf(" %d=%c",i,i);
}



void prepare_text(char *text)
{
	int offset =0; //needed to keep track of real position in case of digraphs
	int i = 0;

	
	while (text[i] != '\0')
	{
		printf("\n'%c' . next char", text[i]);
		
		text[i] = toupper(text[i]);
		text[i+1] = toupper(text[i+1]);
		
		if (!isalpha(text[i]) && !isInterpunct(text[i]))
		{
			printf("\n'%c' not a valid character. skipping\n", text[i]);
			offset++;
			i++;
			continue;
		}
		
		if ((text[i] ==' ') || (text[i] ==',') || (text[i] =='.'))
		{
			printf("\nChanging '%c' to silence\n", text[i]);
			text[i-offset] = 48; // '0'=48, but it doesnt work for some reason
		}
		else
		if (text[i] =='N' && text[i+1] =='J')
		{
			text[i-offset] = '{';
			++i;
			++offset;
			printf("\nChanging NJ to {\n");
		}
		else
		if (text[i] =='L' && text[i+1] =='J')
		{
			text[i-offset] = '|';
			++offset;
			++i;
			printf("\nChanging LJ to |\n");
		}
		else
		if (text[i] =='D' && text[i+1] =='Ž')
		{
			text[i-offset] = '#';
			++i;
			++offset;
			printf("\nChanging NJ to #\n");
		}
		else
		if (text[i] =='Đ')
		{
			text[i-offset] = '$';
			printf("\nChanging Đ to $\n");
		}
		else
			text[i-offset] = text[i];
		
		++i;
	}
	text[i-offset] = '\0';
}

FILE *catLPCs(char *phons)
{
	FILE *output;
	output = fopen ("lpc_concatenated.lpc", "wb");
	char path[10];
	
	//find each lpc file and write it to output
	int i = 0;
	while(phons[i] != '\0')
	{
		path[0] = phons[i];
		path[1] = '/';
		path[2] = phons[i];
		path[3] = '\0';
		strcat(path, ".lpc\0");
		
		printf("\nReading from file: %s", path);
		write_lpc(path, output);
		i++;
	}
	
	return output;
}

unsigned int write_lpc (char *path, FILE *output)
{
	FILE * input = fopen(path, "rb");
	int buffer;
	
	while (1)
	{	
		buffer = fgetc(input); 
		if(buffer == EOF) break;
		fputc((char)buffer, output);
	}
	
	fclose(input);
	
	return 0;
}

FILE *catPitches(char *phons)
{
	FILE *output;
	output = fopen ("pitches_concatenated.pitch", "wb");
	char path[10];
	
	//find each lpc file and write it to output
	int i = 0;
	while(phons[i] != '\0')
	{
		path[0] = phons[i];
		path[1] = '/';
		path[2] = phons[i];
		path[3] = '\0';
		strcat(path, ".pitch\0");
		
		printf("\nReading from file: %s", path);
		write_pitch(path, output);
		i++;
	}
	
	return output;
}

unsigned int write_pitch (char *path, FILE *output)
{
	FILE * input = fopen(path, "rb");
	int filesize;
	float buffer;
	
	fseek( input, 0L, SEEK_END);
	filesize = ftell(input)/4;
	fseek( input, 0L, SEEK_SET);
	
	for(int i=0; i<filesize; i++)
	{
		fread( &buffer, 1, sizeof(buffer), input);
		if((int)buffer == EOF) break;
		if(path[0] != '0') buffer = 80.0;
		fwrite( &buffer, 1, sizeof(buffer), output);
	}
	
	fclose(input);
	
	return 0;
}

unsigned int write_phoneme (char *phoneme, SNDFILE *outfile)
{
	SNDFILE *infile;
	SF_INFO sfinfo_orig = {};
	SF_INFO* sfinfo = &sfinfo_orig; 
	
	int readcount=1;
	char phoneme_filename[6];
	static double data [BUFFER_LEN] ;
	
	if (!isPhoneme(phoneme))
	{
		printf("\nError!. %s is not a phoneme. Exiting current write.\n", phoneme);
		return 0;
	}
	
	strcpy(phoneme_filename, phoneme);
	strcat(phoneme_filename, ".wav");
	
	if (! (infile = sf_open (phoneme_filename, SFM_READ, sfinfo)))
    {   
		/* Open failed so print an error message. */
        printf ("Not able to open input file.\n") ;
        /* Print the error message fron libsndfile. */
        sf_perror (NULL) ;
        return  1 ;
	}
	else
		printf("\nInput file read sucessfully...");

	while ((readcount = sf_read_double (infile, data, BUFFER_LEN)))
    {   
        sf_write_double (outfile, data, readcount) ;
	};
	sf_close(infile);
	
	return 1;
}


int isInterpunct(char c)
{
	if(c == ' ' || c == ',' || c=='.')
		return 1;
	else 
		return 0;
}

int isPhoneme(char *phon)
{
	//printf("\nChecking if %s is a phoneme...", phon);
	int i = 0;
    while(phonemes[i] != NULL)
    {
        if(!strcmp(phonemes[i], phon))
		{
			//printf("\nIt is...\n");
			return 1;
		}
		i++;
    }
    
	//printf("\nIt isnt\n");
    return 0;
}

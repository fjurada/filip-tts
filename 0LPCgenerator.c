#include <stdio.h>
#include <stdlib.h>
#include <sndfile.h>
#include <string.h>
#include <unistd.h>



int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("Not enough arguments! \n");
		return -1;
	}
	
	char command[100];
	char file[20];
	
	int i;
	for (i=1; i < argc; i++)
	{
		printf("------- Arg %d : %s ------ \n", i, argv[i]);
	
		
		strcpy(file, argv[i]);
		
		int j=0;
		
		while (file[j] != '.')
			j++;
		
		file[j] = '\0';
		
		printf("\nProcessing %s", argv[i]);
		printf("\nMaking Directory...");
		strcpy(command, "mkdir ");
		strcat(command, file);
		printf("\nCommand: %s", command);
		system(command);
		
		printf("\nConverting to raw...");
		strcpy(command, "dd if=");
		strcat(command, argv[i]);
		strcat(command, " bs=44 skip=1 of=");
		strcat(command, file);
		strcat(command, "/");
		strcat(command, file);
		strcat(command, ".short");
		printf("\nCommand: %s", command);
		system(command);
		
		printf("\nConverting to float and calculating LPC...");
		strcpy(command, "x2x +sf < ");
		strcat(command, file);
		strcat(command, "/");
		strcat(command, file);
		strcat(command, ".short | frame -l 800 -p 80 | window -l 800 -n 1 | lpc -l 800 -m 50 > ");
		strcat(command, file);
		strcat(command, "/");
		strcat(command, file);
		strcat(command, ".lpc");
		printf("\nCommand: %s", command);
		system(command);
		
		
		printf("\nGetting Pitch data...");
		strcpy(command, "x2x +sf ");
		strcat(command, file);
		strcat(command, "/");
		strcat(command, file);
		strcat(command, ".short | pitch -a 1 -s 44.1 -p 80 -L 60 -H 240 > ");
		strcat(command, file);
		strcat(command, "/");
		strcat(command, file);
		strcat(command, ".pitch");
		printf("\nCommand: %s", command);
		system(command);
		
		
		printf("\nPlotting Pitch data...");
		strcpy(command, "fdrw -y 0 500 -W 1.5 -H 0.5 < ");
		strcat(command, file);
		strcat(command, "/");
		strcat(command, file);
		strcat(command, ".pitch | psgr > ");
		strcat(command, file);
		strcat(command, "/");
		strcat(command, file);
		strcat(command, "-pitch.eps");
		printf("\nCommand: %s", command);
		system(command);
		
		printf("\nFilename: %s\n",file);
	}
	
	printf("END\n\n");
		
	return 0;
}
